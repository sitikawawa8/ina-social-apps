import React, {useState, useEffect} from 'react'
import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import Posts from '../components/Posts'
import axios from 'axios'
import Comments from '../components/Comments'

export default function Home(props) {

  // Not Using This Anymore

  const [userName, setUserName] = useState([])
  const [userPosts, setUserPost] = useState([])
  const [postComments, setpostComments] = useState({})




  const fetchData = () => {
    const usernameAPI = 'https://jsonplaceholder.typicode.com/users';
    const userCommentsAPI = 'https://jsonplaceholder.typicode.com/comments';
    const userPostsAPI = 'https://jsonplaceholder.typicode.com/posts';
    const getAllUsername = axios.get(usernameAPI)
    const getAllUserPosts = axios.get(userPostsAPI)

    axios.all([getAllUsername, getAllUserPosts]).then(
      axios.spread(async(...allData) => {
        const allUsernameData = allData[0].data
        const allUserPostData = allData[1].data
        setUserName(allUsernameData)
        setUserPost(allUserPostData)
      })
    )
  }

  useEffect(() => {
    fetchData()
  }, [])



  useEffect(() => {
     const commentsec = {}
     
  const viewComments = async(id) => {
    const data = await axios.get(`https://jsonplaceholder.typicode.com/posts/${id}/comments`)
    return data
   }
        for (let index = 0; index < userPosts.length; index++) {
          const element = userPosts[index];
          const getAllUserComments = viewComments(element.id)
          // const data = await getAllUserComments
          commentsec[element.id] = getAllUserComments
        }
        setpostComments(commentsec)
        
        // console.log('KOMENGSEC',commentsec)
  }, [userPosts])
  
// console.log('PostCom',postComments)
    return (
        <div className={styles.container}>
            <Head>
                <title>Ina Social Apps</title>
                <meta name="description" content="kakakwawa95 using next.js"/>
                <link rel="icon" href="/favicon.ico"/>
            </Head>

            <main className={styles.main}>
                <h1 className={styles.title}>
                    Welcome to
                    <a href="https://inaproduct.com/"> Ina Social Apps</a>
                </h1>

                <p className={styles.description}>
                    all this exercise was created by Muhammad Taqwa Aziz
                </p>
                
                {/* Posts */}
                <div className={styles.grid}>
                    <span className={styles.card} >
                    {/* <div id="users"></div> */}
                       {userPosts.map((e, i) => {
                        let name = ""
                        for (let index = 0; index < userName.length; index++) {
                          const element = userName[index];
                          if (element.id == e.userId) {
                            name = element.name
                          }
                        }
                        return (
                          <div 
                          key={i}>
                          <Posts
                          userId={name}
                          id={e.userId}
                          title={e.title}
                          bodyPosts={e.body}
                          />
                          <div style={{textAlign: 'center'}}>
                            <h3>Here are all the comments</h3>
                          </div>
                          <br/>
                          <code>
                            <Comments
                          comments={postComments[e.id]}
                          />
                          </code>
                         
                          </div>
                        )
                       
})}
                       
                    </span>
                    <hr/>
                </div>
            </main>

            <footer className={styles.footer}>
                <a
                    href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
                    target="_blank"
                    rel="noopener noreferrer">
                    Powered by{' '}
                    <span className={styles.logo}>
                        <Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16}/>
                    </span>
                </a>
            </footer>
        </div>
    )
}
