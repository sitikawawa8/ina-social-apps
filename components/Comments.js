import { useEffect, useState } from 'react'
import styles from '../styles/Home.module.css'

const Comments = (props) => {
    const [commentList, setCommentList] = useState([])
    useEffect(() => {
        if (props.comments) {
            props.comments.then(res => {setCommentList(res.data)})
        }
      }, [props.comments])
    
    // console.log('CL',commentList)
    // props.comments.then(res => <p>{res.body}</p>)
    return (
        <div>
             {commentList.map((e, i) => (
                <div  key={i}>
                    <h3>Comments from <span style={{color : 'blueviolet'}}>{e.email}</span></h3>
                    <p>
                    {e.body}
                    </p>
                    
            <hr/>
                </div>
             ))}
            
            {/* Comments */}
            <br/>
            <br/>
        </div>
    )

}

export default Comments