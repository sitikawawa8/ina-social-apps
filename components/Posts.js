import styles from '../styles/Home.module.css'

const Posts = (props) => (
    <div>
        {/* <a>Post No #{props.id}</a> */}
        <h2>
           Posted by: {props.userId}
        </h2>
        <h3>{props.title}</h3>
        <p>
        {props.bodyPosts}
        </p>
        {/* Comments */}
        <br/>
    </div>
)

export default Posts